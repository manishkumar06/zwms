import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ZwmsDashboardRoutingModule } from './zwms-dashboard-routing.module';
import { ZwmsDashboardComponent } from './components/zwms-dashboard/zwms-dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SubFooterComponent } from './components/sub-footer/sub-footer.component';


@NgModule({
  declarations: [
    ZwmsDashboardComponent,
    HeaderComponent,
    FooterComponent,
    SubFooterComponent
  ],
  imports: [
    CommonModule,
    ZwmsDashboardRoutingModule
  ]
})
export class ZwmsDashboardModule { }
