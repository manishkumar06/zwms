import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZwmsDashboardComponent } from './zwms-dashboard.component';

describe('ZwmsDashboardComponent', () => {
  let component: ZwmsDashboardComponent;
  let fixture: ComponentFixture<ZwmsDashboardComponent>;

  beforeEach((() => {
    TestBed.configureTestingModule({
      declarations: [ ZwmsDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZwmsDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
