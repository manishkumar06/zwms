import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ZwmsDashboardComponent } from './components/zwms-dashboard/zwms-dashboard.component';


const routes: Routes = [
  {
    path: '',
    component: ZwmsDashboardComponent,
    children: [
      {
        path: 'home',
        loadChildren: () => import('./sub-features/home/home.module')
          .then(m => m.HomeModule)
      },
      {
        path: 'gallery',
        loadChildren: () => import('./sub-features/gallery/gallery.module')
          .then(m => m.GalleryModule)
      },
      {
        path: 'contactUs',
        loadChildren: () => import('./sub-features/contact-us/contact-us.module')
          .then(m => m.ContactUsModule)
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ZwmsDashboardRoutingModule { }
