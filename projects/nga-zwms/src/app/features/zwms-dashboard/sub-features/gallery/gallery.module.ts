import { GalleryComponent } from './components/gallery/gallery.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasonryComponent } from './components/masonry/masonry.component';
import { GalleryRoutingModule } from './gallery-routing.module';
import {GalleriaModule} from 'primeng/galleria';


@NgModule({
  declarations: [GalleryComponent, MasonryComponent],
  imports: [
    CommonModule,
    GalleryRoutingModule,
    GalleriaModule
  ]
})
export class GalleryModule { }
