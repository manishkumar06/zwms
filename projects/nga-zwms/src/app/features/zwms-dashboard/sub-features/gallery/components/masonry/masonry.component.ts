import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'zwms-masonry',
  templateUrl: './masonry.component.html',
  styleUrls: ['./masonry.component.scss']
})
export class MasonryComponent implements OnInit {
  images: any[];

  responsiveOptions: any[] = [
    {
      breakpoint: '1024px',
      numVisible: 5
    },
    {
      breakpoint: '768px',
      numVisible: 3
    },
    {
      breakpoint: '560px',
      numVisible: 1
    }
  ];

  responsiveOptions2: any[] = [
    {
      breakpoint: '1500px',
      numVisible: 5
    },
    {
      breakpoint: '1024px',
      numVisible: 3
    },
    {
      breakpoint: '768px',
      numVisible: 2
    },
    {
      breakpoint: '560px',
      numVisible: 1
    }
  ];

  displayBasic: boolean;

  displayBasic2: boolean;

  displayCustom: boolean;

  activeIndex = 0;

  ngOnInit(): void {
    this.images = [
      {
        thumbnailImageSrc: 'https://www.zerowastems.com/wp-content/uploads/2020/02/16.jpg',
        previewImageSrc: 'https://www.zerowastems.com/wp-content/uploads/2020/02/16.jpg'
      },
      {
        thumbnailImageSrc: 'https://www.zerowastems.com/wp-content/uploads/2020/02/WhatsApp-Image-2020-02-04-at-11.12.41-AM.jpeg',
        previewImageSrc: 'https://www.zerowastems.com/wp-content/uploads/2020/02/WhatsApp-Image-2020-02-04-at-11.12.41-AM.jpeg'
      },
      {
        thumbnailImageSrc: 'https://www.zerowastems.com/wp-content/uploads/2020/02/15.jpg',
        previewImageSrc: 'https://www.zerowastems.com/wp-content/uploads/2020/02/15.jpg'
      }
    ];
  }

  imageClick(index: number): void {
    this.activeIndex = index;
    this.displayCustom = true;
  }
}
