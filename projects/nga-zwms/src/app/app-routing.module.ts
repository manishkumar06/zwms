import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './components/main/main.component';


const routes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('./features/zwms-dashboard/zwms-dashboard.module')
      .then(m => m.ZwmsDashboardModule)
  },
  {
    path: '',
    component: MainComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
