import { TestBed } from '@angular/core/testing';

import { NgpZwmsDsService } from './ngp-zwms-ds.service';

describe('NgpZwmsDsService', () => {
  let service: NgpZwmsDsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NgpZwmsDsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
