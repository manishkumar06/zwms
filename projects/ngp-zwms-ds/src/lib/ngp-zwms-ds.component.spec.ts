import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgpZwmsDsComponent } from './ngp-zwms-ds.component';

describe('NgpZwmsDsComponent', () => {
  let component: NgpZwmsDsComponent;
  let fixture: ComponentFixture<NgpZwmsDsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgpZwmsDsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgpZwmsDsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
