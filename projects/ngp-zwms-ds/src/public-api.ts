/*
 * Public API Surface of ngp-zwms-ds
 */

export * from './lib/ngp-zwms-ds.service';
export * from './lib/ngp-zwms-ds.component';
export * from './lib/ngp-zwms-ds.module';
